# chapter1

## C++與演算法 黃佑仁(Rock) 很好的範本
國際資訊奧林匹亞台灣種子選手，推薦進入臺灣大學資訊工程學系
國立臺灣大學中等教育學程
中等教育資訊科技概論科教師
多年程式設計教學與競賽指導教師
多項資訊專案開發工程師
https://www.csie.ntu.edu.tw/~b98902112/cpp_and_algo/index.html

## 教材設計
NTHU Online Judge Solution
https://hackmd.io/@Ice1187/nthuoj-solution

https://github.com/loyihsu/nthu-oldjay
Problem: http://140.114.86.238/problem/1000/

:::warning
11160 - GCD and LCM
:::
http://140.114.86.238/problem/11160/
https://hackmd.io/WgxDCn6FTqarH2QjiG2LBA

:::warning
11099 - Money Money  
:::
http://140.114.86.238/problem/11099/
https://github.com/loyihsu/nthu-oldjay/blob/master/11099%20-%20Money%20Money/main.c

:::warning
10767 - Find the maximum/minimum values
:::
http://140.114.86.238/problem/10767/
3 3
1 3 2
4 6 5
7 9 8
https://github.com/loyihsu/nthu-oldjay/blob/master/10767%20-%20Find%20the%20maximum%26minimum%20values/main.c

:::warning
10741 - The Encoded Word  
:::
https://github.com/loyihsu/nthu-oldjay/blob/master/10741%20-%20The%20Encoded%20Word/main.c
First digit =>  even: 'A', odd: 'B'
Second digit => even: 'C', odd: 'D'
Third digit => even: 'E', odd: 'F'

577(A three-digit integer consisting of only 1-9 but not 0)

BDF

:::warning
10766 - Shell Game 
:::
For example, k=3, and three swaps are
0 4
2 3
1 2
 
In this problem, there are five balls, numbered 0-4.
Initially, the order of balls is: 0 1 2 3 4. After the swap (0,4), the order of balls becomes: 4 1 2 3 0; After the swap (2,3), the order of balls becomes: 4 1 3 2 0; and after the swap (1,2), the order of balls becomes:

4 3 1 2 0. So your program needs to print
4 3 1 2 0

3
0 4
2 3
1 2

4 3 1 2 0

## 資料結構與演算法/leetcode/lintcode題解
https://algorithm.yuanbin.me/zh-tw/

### 飆程式網!
http://khcode.m-school.tw/